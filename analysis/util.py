#!/usr/bin/env python
import guess_language as guess
import json
import matplotlib.pyplot as plt
import math
from scipy import stats
import copy

def load_stat(lang, dir='../../wda_data/stat/') :
    data = open(dir+lang+'.stat', 'r')
    str = data.read()
    stat = json.loads(str, encoding="utf-8")
    data.close()
    return stat

def __init_value__(obj) :
    init_val = 0.0
    if type(obj) == type(u''):
        init_val = u''
    elif type(obj) == type(0):
        init_val = 0
    return init_val

def array_of(terms,attr) :
    keys = terms.keys()
    init_val = __init_value__(terms[keys[0]][attr])
    data = [ init_val for i in range(len(terms)) ]
    for i in range(len(terms)):
        data[i] = terms[keys[i]][attr]
    return data

def normalize(list) :
    items = copy.copy(list)
    min_val = min(items)
    for i in range(len(items)) :
        items[i] -= min_val
    max_val = max(items)
    for i in range(len(items)) :
        items[i] /= max_val
    return items

def encode(values, encoding='utf-8') :
    vals = ['' for i in range(len(values))]
    for i in range(len(values)):
        vals[i] = values[i].encode(encoding)
    return vals

def log(values, base=math.e) :
    log_vals = [0.0 for i in range(len(values))]
    for i in range(len(values)):
        log_vals[i] = math.log(values[i],base)
    return log_vals

def guess_language(text) :
    return guess.guessLanguageName(text)

def spearman(x,y) :
    return stats.spearmanr(x,y)
    
def pearson (x,y) :
    return stats.pearsonr(x, y)
    
def sort_obj(dict, attr, desc = False) :
    return sorted(dict.values(), key=lambda x:x[attr], reverse=desc)

def simple_scatter_plot(x,y,x_label='x', y_label='y', title='') :
    fig = plt.figure()
    ax = fig.add_subplot(111)
    p = ax.scatter(x, y, s=5, color='r', edgecolor='none')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(title)
    plt.show()
    