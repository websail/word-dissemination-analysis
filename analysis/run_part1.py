#!/usr/bin/env python
# coding=UTF-8
import util
import sys
import numpy as np
import matplotlib.pyplot as plt

m_names = ['IDF', 'rIDF', 'IDF2', 'rIDF2', 'Burstiness', '+adapt2', '+adapt3']
m_attrs = ['idf', 'rIDF', 'idf2', 'rIDF2', 'burstiness', 'adapt2', 'adapt3']
m_stats = {}


def cal_all_correlation_coeffs(tf, stats, func) :
    coeffs = []
    p_vals = []
    for attr in m_attrs :
        coeff, p_val = func(tf, stats[attr])
        coeffs.append(abs(coeff))
        p_vals.append(p_val)
    return coeffs, p_vals

def get_all_stats(terms):
    stats = {}
    for attr in m_attrs :
        stat = util.array_of(terms, attr)
        if attr != 'adapt2' or attr != 'adapt3' :
            stat = util.normalize(stat)
        stats[attr] = stat
    return stats

def print_top_terms(terms, attr, name, top=10, df_threshold=10, desc=True) :
    order = 'Top'
    if not desc: order = 'Bottom'
    print('----------%s %d %s-----------' % (order, top, name))
    sorted_list = util.sort_obj(terms, attr, desc)
    max_val = sorted_list[0][attr]
    min_val = sorted_list[-1][attr]
    i = 0
    j = 0
    while i < top and j < len(sorted_list):
        obj = sorted_list[j]
        #avoid smoothing
        if obj['df1'] > df_threshold and obj['df2'] > 0 and obj['df3'] > 0 : #and ( obj[attr] < max_val or not desc):
            print_term(obj)
            i+=1
        j += 1
    
    
def print_term(obj):
    print '''%s,\t tf: %d, df1: %d, df2: %d, idf: %f, rIDF: %f, idf2: %f, rIDF2: %f, burstiness: %f, adapt2: %f, adapt3: %f''' % (obj['term'].encode('utf-8'), obj['tf'], obj['df1'], obj['df2'], obj['idf'], obj['rIDF'], obj['idf2'], obj['rIDF2'], obj['burstiness'], obj['adapt2'], obj['adapt3'])

def print_2_coeffs(p_coeffs, s_coeffs) :
    print "\tpearson\t\tspearman"
    for i in range(len(m_names)) :
        name = m_names[i]
        print('%s\t%f\t\t%f'% (name, p_coeffs[i], s_coeffs[i]))

def plot_scatter(x,y,x_label,y_label,title=None) :
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    p1 = ax1.scatter(x, y, s=3, color='b', alpha=1, edgecolor='none')
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    if title is None:
        title = x_label + ' ' + y_label
    plt.title(title)


def main(language, plot=True, top=10):
    stat = util.load_stat(language)
    terms = stat['terms']
    words = util.encode(util.array_of(terms, 'term'))
    tf = util.array_of(terms, 'tf')
    df = util.array_of(terms, 'df1')
    log_df = util.log(df, 2)
    log_tf = util.log(tf, 2)
    m_stats = get_all_stats(terms)
    
    pearson_coeffs, pearson_pvals = cal_all_correlation_coeffs(tf, m_stats, util.pearson)
    print pearson_pvals
    spearman_coeffs, spearman_pvals = cal_all_correlation_coeffs(tf, m_stats, util.spearman)
    print spearman_pvals
    print_2_coeffs(pearson_coeffs, spearman_coeffs)
   
    xlabel = 'Log Term Frequency'
    if plot :
        for i in range(len(m_attrs)) :
            attr = m_attrs[i]
            name = m_names[i]
            plot_scatter(log_tf, m_stats[attr], xlabel, name)
        
        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        width = 0.35
        ind = np.arange(7) 
        p6 = ax1.bar(ind, pearson_coeffs, width, color='b')
        p7 = ax1.bar(ind+width, spearman_coeffs, width, color='g')
        ax1.set_ylabel('absolute coeff')
        ax1.set_xticks(ind+width)
        ax1.set_xticklabels(m_names)
        ax1.legend( (p6[0], p7[0]), ('pearson', 'spearman') )
        plt.title('Correlation Coefficients')
        plt.show()
        
    df_threshold = 10
    if top > 0 :
        for i in range(len(m_attrs)) :
            attr = m_attrs[i]
            name = m_names[i]
            print_top_terms(terms, attr, name, df_threshold=df_threshold, top=top)
    return terms
    
        
if __name__ == '__main__':
    terms = main(sys.argv[1], plot=(sys.argv[2]=='True'), top=int(sys.argv[3]))
    
