#!/usr/bin/env python
# coding=UTF-8
import util as util
import run_part1 as util2
import sys
import numpy as np
import math

def range_num(log_tf) :
    if log_tf < 5 : return 0
    if 5 <= log_tf and log_tf < 10: return 1
    if 10 <= log_tf and log_tf < 15: return 2
    else: return 3 

def main(language, attr, top) :
    stat = util.load_stat(language)
    terms = stat['terms']
    term_sets = [{},{},{},{}]
    group_names = ['log(tf) < 5', '5 <= log(tf) < 10', '10 <= log(tf) < 15', '15 <= log(tf)']
    
    #divide terms into 4 sets
    # log(tf) < 5, 5 <= log(tf) < 10 ,10 <= log(tf) < 15, 15 <= log(tf)
    for term in terms.keys():
        log_tf = math.log(terms[term]['tf'], 2)
        group = range_num(log_tf)
        term_sets[group][term] = terms[term]
    
    print '# of terms in each group'
    for i in range(len(term_sets)) :
        print ('%s: %d' % (group_names[i], len(term_sets[i])))
    
    
    #top attr of each group
    if top > 0:
        for i in range(len(term_sets)) :
            set = term_sets[i]
            name = attr + ' in ' + group_names[i]
            util2.print_top_terms(set, attr, name, df_threshold=10, top = top)
            util2.print_top_terms(set, attr, name, df_threshold=10, desc=False, top = top)
    
    # 3 for proper name, 3 for potential content words, 3 for general words, 3 for function words
    # expected values high -----> low
    #selected few terms for Thai
    if language == 'th':
        print 'proper names'
        util2.print_term(terms[u'เจได'])
        util2.print_term(terms[u'อุดรธานี'])
        util2.print_term(terms[u'มหิดล'])
        print 'potential content words'
        util2.print_term(terms[u'อะตอม'])
        util2.print_term(terms[u'สะเทิน'])
        util2.print_term(terms[u'ธรรมาภิสมัย'])
        print 'common words'
        util2.print_term(terms[u'สุข'])
        util2.print_term(terms[u'เริ่มแรก'])
        util2.print_term(terms[u'สับสน'])
        print 'function words'
        util2.print_term(terms[u'ที่'])
        util2.print_term(terms[u'เป็น'])
        util2.print_term(terms[u'จาก'])
        
    if language == 'simple':
        print 'proper names'
        util2.print_term(terms[u'jedi'])
        util2.print_term(terms[u'chicago'])
        util2.print_term(terms[u'stanford'])
        print 'potential content words'
        util2.print_term(terms[u'atom'])
        util2.print_term(terms[u'statistics'])
        util2.print_term(terms[u'biofuel'])
        print 'common words'
        util2.print_term(terms[u'happy'])
        util2.print_term(terms[u'beginning'])
        util2.print_term(terms[u'confusing'])
        print 'function words'
        util2.print_term(terms[u'at'])
        util2.print_term(terms[u'is'])
        util2.print_term(terms[u'from'])
    

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2], top=int(sys.argv[3]))
    