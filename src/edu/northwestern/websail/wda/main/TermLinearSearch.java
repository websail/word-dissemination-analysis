package edu.northwestern.websail.wda.main;

import java.io.IOException;

import org.apache.lucene.index.DocsEnum;

import edu.northwestern.websail.wda.data.index.IndexReader;
import edu.northwestern.websail.wda.model.CorpusStat;


public class TermLinearSearch {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String lang = "simple";
		String term = "rgb";
		int tfThreshold = 12;
		IndexReader idr = new IndexReader();
		idr.initialize("../wda_data/index", lang);
		CorpusStat stat = new CorpusStat(lang, idr.getCurrentTotalDocs());
		int count = 0;
		while(idr.nextTerm()) {
			
			if(idr.getCurrentTermStat().getTf() < tfThreshold) continue;
			count++;
			 stat.addTerm(idr.getCurrentTermStat());
			 if(count %1000 == 0) {
				 //System.out.println("processing: "+count + ", last term: "+idr.getCurrentTermStat().getTerm());
			 }
			 if(idr.getCurrentTermStat().getTerm().equals(term)) {
				 DocsEnum docs = idr.getCurrentIterator().docs(null, null);
		        while(docs.nextDoc() != DocsEnum.NO_MORE_DOCS) {
		        	System.out.println(docs.docID());
		        }
			 }
		}
		idr.close();
		System.out.println(stat.getTotalDocs());
		System.out.println(stat.getTotalTerms());
		System.out.println(stat.getTotalTermFrequency());
//		Gson gson = new Gson();
//		System.out.println(gson.toJson(stat.getTerms().get(0)));
//		String statStr = gson.toJson(stat);
//		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
//			    new FileOutputStream("../wda_data/stat/"+lang+".stat"), "UTF-8"));
//		out.write(statStr);
//		out.flush();
//		out.close();

	}

}
