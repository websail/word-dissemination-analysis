package edu.northwestern.websail.wda.main;

import java.io.IOException;

import edu.northwestern.websail.wda.model.CorpusStat;
import edu.northwestern.websail.wda.util.CorpusStatSerializer;

public class TestStatReadWrite {

	public static void main(String[] args) throws IOException {

		String fileName = args[0];

		CorpusStat cs = CorpusStatSerializer.deserialize(fileName);

		System.out.println(cs.getName());
		System.out.println(cs.getTotalDocs());
		System.out.println(cs.getTotalTermFrequency());
		System.out.println(cs.getTotalTerms());

	}

}
