package edu.northwestern.websail.wda.main;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import edu.northwestern.websail.wda.model.CorpusStat;
import edu.northwestern.websail.wda.model.CorpusStatLM;
import edu.northwestern.websail.wda.model.TermMeasureStat;
import edu.northwestern.websail.wda.util.CorpusStatRandFileSerializer;
import edu.northwestern.websail.wda.util.CorpusStatSerializer;

public class ConvertStatFile {

	public static void main(String[] args) throws IOException {

		String dirName = args[0];
		String statFileName = args[1];
		String outputStatFile = args[2];

		CorpusStatLM cslm = CorpusStatRandFileSerializer
				.deserializeFromRandomAccessFile(dirName, statFileName);

		CorpusStat stat = new CorpusStat(cslm.getName(), cslm.getTotalDocs());

		HashMap<String, TermMeasureStat> terms = new HashMap<String, TermMeasureStat>();
		HashMap<String, Long> termPostitions = cslm.getKeyPositionsMap();
		for (Entry<String, Long> termPos : termPostitions.entrySet()) {
			terms.put(termPos.getKey(),
					cslm.getTermMeasureStat(termPos.getKey()));
		}
		stat.setTotalTermFrequency(cslm.getTotalTermFrequency());
		stat.setTotalTerms(cslm.getTotalTerms());
		stat.setTerms(terms);
		stat.setTotalDocs(cslm.getTotalDocs());

		CorpusStatSerializer.serialize(stat, outputStatFile);

	}

}
