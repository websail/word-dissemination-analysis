package edu.northwestern.websail.wda.main;

import java.io.IOException;

import edu.northwestern.websail.wda.util.SWATHAdapter;


public class TestSWATH {

	/**
	 * @param args
	 * @throws InterruptedException 
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		String out = SWATHAdapter.swath("ประเทศไทยมีบริการเทเลเท็กซ์โดยไม่คิดมูลค่าทางช่อง5 มานานกว่า 4 ปีแล้ว", true, "long", " ");
		System.out.println(out);
	}

}
