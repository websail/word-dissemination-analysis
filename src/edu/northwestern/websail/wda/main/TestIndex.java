package edu.northwestern.websail.wda.main;

import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.util.Version;


public class TestIndex {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		//Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40, emptyStopWords());
		
		
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40, new CharArraySet(Version.LUCENE_40, 0, false));
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
		
		System.out.println(config.getOpenMode());
		//store index in a file for later use
//	    File indexFile = new File("../wda_data/index/ng");
//	    Directory directory = FSDirectory.open(indexFile);
//	    
//	    DirectoryReader reader = DirectoryReader.open(directory);
//	    System.out.println(reader.numDocs());
	    
	    
//	    IndexSearcher searcher = new IndexSearcher(reader);
//		Query q = NumericRangeQuery.newIntRange("id", 20354, 20354, true, true);
//		TopDocs hits = searcher.search(q, 10);
//		if(hits.scoreDocs.length == 0) {
//			System.out.println("no doc");
//		}
//		System.out.println(hits.scoreDocs.length);
//		String article = reader.document(hits.scoreDocs[1].doc).get("article");
//	    String title = reader.document(hits.scoreDocs[1].doc).get("title");
//	    System.out.println(title);
//	    System.out.println(article);
//	    reader.close();
		
		
		
		
	}
	
	public static CharArraySet emptyStopWords() {
		CharArraySet set = new CharArraySet(Version.LUCENE_40, 0, false);
		return set;
	}

}
