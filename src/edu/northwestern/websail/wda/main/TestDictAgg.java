package edu.northwestern.websail.wda.main;

import java.io.IOException;

import edu.northwestern.websail.wda.data.dict.ConcurrentDictAggregator;

public class TestDictAgg {
	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		ConcurrentDictAggregator dAgg = new ConcurrentDictAggregator("test");
		dAgg.initEmptyAggregator("../wda_data/dict");
		dAgg.setVerbose(true);
		//dAgg.processWikiDump("../wda_data/dump/ngwiki-20130605-pages-articles.xml", "../wda_data/dict");
		dAgg.addPlaintextDocument(0, "title", "test frequency words test");
		dAgg.close();
	}

}
