package edu.northwestern.websail.wda.main;

import edu.northwestern.websail.wda.model.CorpusStat;
import edu.northwestern.websail.wda.model.CorpusStatLM;
import edu.northwestern.websail.wda.util.CorpusStatRandFileSerializer;
import edu.northwestern.websail.wda.util.CorpusStatSerializer;

import java.io.File;
import java.io.IOException;

/**
 * User: csbhagav
 * Date: 9/24/13
 */
public class RAFSerializer {
    public static void main(String[] args) throws IOException, ClassNotFoundException {

//        "/home/csbhagav/wikification/data/statSerializer/contexts.stat";
        String fileName = args[0];
        String outputDirName = args[1];

        File statFile = new File(fileName);
        CorpusStat cs = CorpusStatSerializer.deserializeBinary(fileName);
        CorpusStatRandFileSerializer.serializeToRandomAccessFiles(cs, outputDirName, statFile.getName());
        CorpusStatLM cslm = CorpusStatRandFileSerializer.deserializeFromRandomAccessFile(outputDirName, statFile.getName());
        System.out.println(cslm.getTermMeasureStat("jerusalem.carroll"));
    }


}
