package edu.northwestern.websail.wda.main;

import java.io.IOException;

import com.google.gson.Gson;

import edu.northwestern.websail.wda.data.Reader;
import edu.northwestern.websail.wda.data.index.IndexReader;
import edu.northwestern.websail.wda.model.CorpusStat;
import edu.northwestern.websail.wda.util.CorpusStatSerializer;


public class ComputeStat {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		String lang = "ng";
		int tfThreshold = 12;
		Reader idr = new IndexReader();
		idr.initialize("../wda_data/index", lang);
		CorpusStat stat = new CorpusStat(lang, idr.getCurrentTotalDocs());
		int count = 0;
		while(idr.nextTerm()) {
			
			if(idr.getCurrentTermStat().getTf() < tfThreshold) continue;
			count++;
			 stat.addTerm(idr.getCurrentTermStat());
			 if(count %1000 == 0) {
				 System.out.println("processing: "+count + ", last term: "+idr.getCurrentTermStat().getTerm());
			 }
		}
		idr.close();
		System.out.println(stat.getTotalDocs());
		System.out.println(stat.getTotalTerms());
		System.out.println(stat.getTotalTermFrequency());
		Gson gson = new Gson();
		System.out.println(gson.toJson(stat.getTerms().get("a")));
		String statFile = "../wda_data/stat/"+lang+".stat";
		CorpusStatSerializer.serialize(stat, statFile);
		
		CorpusStat stat2 = CorpusStatSerializer.deserialize(statFile);
		System.out.println(stat2.getTotalDocs());
		System.out.println(stat2.getTotalTerms());
		System.out.println(stat2.getTotalTermFrequency());
		System.out.println(gson.toJson(stat2.getTerms().get("a")));
		
	}

}
