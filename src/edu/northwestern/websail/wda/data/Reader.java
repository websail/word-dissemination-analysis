package edu.northwestern.websail.wda.data;

import java.io.IOException;

import edu.northwestern.websail.wda.model.TermStat;

public abstract class Reader {

	public Reader() {}
	public abstract void initialize(String directory, String language) throws IOException;
	public abstract boolean nextTerm() throws IOException;
	public abstract TermStat getCurrentTermStat() throws IOException;
	public abstract void close() throws IOException;
	public abstract int getCurrentTotalDocs();
}
