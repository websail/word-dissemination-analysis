package edu.northwestern.websail.wda.data.dict;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import com.google.gson.Gson;

import edu.northwestern.websail.wda.data.Aggregator;
import edu.northwestern.websail.wda.model.TermStat;

public class DictAggregator extends Aggregator {

	private File dictFile;
	private File numFile;
	private HashMap<String, TermStat> dict;
	private Analyzer analyzer;
	private TokenStream tokenStream;
	private CharTermAttribute charTermAttribute;
	protected CharArraySet stopWords;
	
	public DictAggregator(String language) {
		super(language);
	}

	public DictAggregator(String language, String stopWordsFile) {
		super(language, stopWordsFile);
	}

	@Override
	public void initEmptyAggregator(String dictDirectory) throws IOException {
		//create empty file
		String dictFileDirectory  = dictDirectory + "/"+lang+"/dict";
		String docNumFileDirectory  = dictDirectory + "/"+lang+"/doc_num";
		dictFile = new File(dictFileDirectory);
		if(dictFileDirectory.indexOf("/") != -1){
			if (!dictFile.getParentFile().exists())
				dictFile.getParentFile().mkdirs();
		}
		if(dictFile.exists()) dictFile.delete();
		dictFile.createNewFile();
		numFile = new File(docNumFileDirectory);
		if(numFile.exists()) numFile.delete();
		numFile.createNewFile();
		
		if(stopWordsFile == null){
			this.stopWords = null;
		} else{
			this.stopWords = this.readStopWords(stopWordsFile, true);
		}
		

		docCount = 0;
		dict = new HashMap<String, TermStat>();
	}

	@Override
	public void addPlaintextDocument(int pageId, String pageTitle,
			String content) throws IOException, InterruptedException {
		if(content.trim().equals("")) return;
		content = this.preprocessContent(content);
		
		this.initTokenizer(content);
		HashMap<String,Integer> tf = this.countTF();
		this.clearOffTokenizer();
		this.addTokenToDict(tf);
		docCount++;
		if(docCount % 1000 == 0 && verbose) System.out.println("processing: " + docCount + ", skip: "+this.docSkip);

	}

	private void initTokenizer(String content) throws IOException{
		//init
		if(this.onlyWhiteSpace) this.analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
		else if(stopWords == null) this.analyzer = new StandardAnalyzer(Version.LUCENE_40, this.emptyStopWords());
		else this.analyzer = new StandardAnalyzer(Version.LUCENE_40, this.stopWords);
		this.tokenStream = analyzer.tokenStream(content, new StringReader(content));
	    this.charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
	}
	
	private void clearOffTokenizer() throws IOException{
		this.tokenStream.close();
		this.analyzer.close();
		this.analyzer = null;
		this.tokenStream = null;
		this.charTermAttribute = null;
	}
	
	private HashMap<String,Integer> countTF() throws IOException{
		HashMap<String, Integer> tf = new HashMap<String, Integer>();
	    while (tokenStream.incrementToken()) {
	    	String tokenText = charTermAttribute.toString();
	        if(!tf.containsKey(tokenText)) tf.put(tokenText, 0);
	        tf.put(tokenText, tf.get(tokenText)+1);
	    }
	    return tf;
	}

	protected void addTokenToDict(HashMap<String, Integer> tf){
		Iterator<Entry<String, Integer>> it = tf.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Integer> pairs = (Map.Entry<String, Integer>)it.next();
	        if(!dict.containsKey(pairs.getKey())) {
	        	TermStat t = new TermStat(pairs.getKey());
	        	dict.put(pairs.getKey(), t);
	        }
	        dict.get(pairs.getKey()).updateDF(pairs.getValue());
	    }
	}
	
	@Override
	public void close() throws IOException {
		Gson gson = new Gson();
		String dictStr = gson.toJson(dict);
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
			    new FileOutputStream(dictFile), "UTF-8"));
		out.write(dictStr);
		out.flush();
		out.close();
		
		String numStr = gson.toJson(docCount);
		BufferedWriter out2 = new BufferedWriter(new OutputStreamWriter(
			    new FileOutputStream(numFile), "UTF-8"));
		out2.write(numStr);
		out2.flush();
		out2.close();
	}

}
