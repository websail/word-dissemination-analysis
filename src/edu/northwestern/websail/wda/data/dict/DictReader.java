package edu.northwestern.websail.wda.data.dict;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.northwestern.websail.wda.data.Reader;
import edu.northwestern.websail.wda.model.TermStat;

public class DictReader extends Reader {

	private int docCount;
	private HashMap<String, TermStat> dict;
	Iterator<Entry<String, TermStat>> it;
	TermStat currentTermStat;
	public DictReader() {
	}

	@Override
	public void initialize(String dictDirectory, String lang)
			throws IOException {
		String dictFileDirectory  = dictDirectory + "/"+lang+"/dict";
		String docNumFileDirectory  = dictDirectory + "/"+lang+"/doc_num";
		Gson gson = new Gson();
		BufferedReader in = new BufferedReader(
				   new InputStreamReader(
		                      new FileInputStream(dictFileDirectory), "UTF8"));
		String line = null;
		String statStr = "";
		while((line = in.readLine()) != null) {
			statStr += line + "\n";
		}
		Type hashType = new TypeToken<HashMap<String, TermStat>>(){}.getType();
		dict = gson.fromJson(statStr, hashType);
		BufferedReader in2= new BufferedReader(new FileReader(docNumFileDirectory));
		line = in2.readLine();
		docCount = gson.fromJson(line, Integer.class);
		it = dict.entrySet().iterator();
		in.close();
		in2.close();
	
	}

	@Override
	public boolean nextTerm() throws IOException {
		boolean has = it.hasNext();
		if(has){
			currentTermStat = it.next().getValue();
		}else
			currentTermStat = null;
		return has;
	}

	@Override
	public TermStat getCurrentTermStat() throws IOException {
		return this.currentTermStat;
	}

	@Override
	public void close() throws IOException {
	}

	@Override
	public int getCurrentTotalDocs() {
		return this.docCount;
	}

}
