package edu.northwestern.websail.wda.data.dict;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

public class ConcurrentDictAggregator extends DictAggregator {
	private Object lock;
	public ConcurrentDictAggregator(String language) {
		super(language);
		this.lock = new Object();
	}

	public ConcurrentDictAggregator(String language, String stopWordsFile) {
		super(language, stopWordsFile);
		this.lock = new Object();
	}
	
	@Override
	public void addPlaintextDocument(int pageId, String pageTitle,
			String content) throws IOException, InterruptedException {
		if(content.trim().equals("")) return;
		content = this.preprocessContent(content);
		
		Analyzer analyzer = null;
		if(stopWords == null) analyzer = new StandardAnalyzer(Version.LUCENE_40, this.emptyStopWords());
		else analyzer = new StandardAnalyzer(Version.LUCENE_40, this.stopWords);
		TokenStream tokenStream = analyzer.tokenStream(content, new StringReader(content));
		CharTermAttribute charTermAttribute = tokenStream.addAttribute(CharTermAttribute.class);
		HashMap<String,Integer> tf = this.countTF(tokenStream, charTermAttribute);
		this.clearOffTokenizer(analyzer, tokenStream, charTermAttribute);
		synchronized(lock) {
			this.addTokenToDict(tf);
			docCount++;
			if(docCount % 1000 == 0 && verbose) System.out.println("processing: " + docCount + ", skip: "+this.docSkip);
		}

	}
	
	private HashMap<String,Integer> countTF(TokenStream tokenStream, CharTermAttribute charTermAttribute) throws IOException{
		HashMap<String, Integer> tf = new HashMap<String, Integer>();
	    while (tokenStream.incrementToken()) {
	    	String tokenText = charTermAttribute.toString();
	        if(!tf.containsKey(tokenText)) tf.put(tokenText, 0);
	        tf.put(tokenText, tf.get(tokenText)+1);
	    }
	    return tf;
	}
	
	private void clearOffTokenizer(Analyzer analyzer, TokenStream tokenStream, CharTermAttribute charTermAttribute) throws IOException{
		tokenStream.close();
		analyzer.close();
		analyzer = null;
		tokenStream = null;
		charTermAttribute = null;
	}

}
