package edu.northwestern.websail.wda.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.util.Version;

import edu.northwestern.websail.wda.util.SWATHAdapter;

public abstract class Aggregator {
	protected boolean useSWATH;
	protected int docCount;
	protected int docSkip;
	protected boolean verbose = false;
	protected String lang;
	protected String stopWordsFile;
	protected boolean onlyWhiteSpace;
	
	public Aggregator(String language) {
		this.lang = language;
		this.useSWATH = false;
		if(language.equals("th")) this.useSWATH = true;
		this.docCount = 0;
		this.docSkip = 0;
		this.stopWordsFile = null;
	}
	
	public Aggregator(String language, String stopWordsFile){
		this(language);
		this.stopWordsFile = stopWordsFile;
	}
	
	@Deprecated
	public void processWikiDump(String filename, String directory) throws IOException{
//		this.initParser();
//		this.initEmptyAggregator(directory);
//		//start parsing XML
//	    ParsingDriver parser = new ParsingDriver(this);
//		Executor.readFile(filename, 2, parser);
	}
	
	public abstract void initEmptyAggregator(String directory) throws IOException;
	public abstract void addPlaintextDocument(int pageId, String pageTitle, String content) throws IOException, InterruptedException ;
	public abstract void close() throws IOException;
	
	@Deprecated
	public void addWikiScriptDocument(int pageId, String pageTitle, String pageContent) throws IOException, InterruptedException{
		if(pageContent.trim().equals("")) return;
		//System.out.println("adding: " + pageTitle);
		String content = this.parsePage(pageContent);
		this.addPlaintextDocument(pageId, pageTitle, content);
	}
	
	public void skip(String title, int namespace, boolean redirect){
		this.docSkip++;
	}
	
	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
	
	
	@Deprecated
	protected String parsePage(String wikiText) throws IOException {
		return wikiText;
	}
	
	protected String preprocessContent(String content) throws IOException, InterruptedException{
		if(this.useSWATH) {
			content = SWATHAdapter.swath(content, true, "long", " ");
			//System.out.println("calling swath");
		}
		return content;
	}
	
	@Deprecated
	protected void initParser(){
//		//init parser
//		MediaWikiParserFactory pf = new MediaWikiParserFactory();
//		pf.setCalculateSrcSpans(true);
//		pf.setShowImageText(false);
//		pf.getImageIdentifers().add("File");
//		pf.getImageIdentifers().add("Image");
//		pf.getImageIdentifers().add("Şəkil");
//		pf.getImageIdentifers().add("ไฟล์");
//		pf.setShowMathTagContent(false);
//		pf.setDeleteTags(true);
//		pf.setTemplateParserClass( FlushTemplates.class );
//		jwplParser = pf.createParser();
//		
//		//set thai tokenizer
//		if(lang.equals("th")) {
//			this.useSWATH = true;
//		}
				
	}
	
	public boolean isOnlyWhiteSpace() {
		return onlyWhiteSpace;
	}

	public void setOnlyWhiteSpace(boolean onlyWhiteSpace) {
		this.onlyWhiteSpace = onlyWhiteSpace;
	}

	protected CharArraySet emptyStopWords() {
		CharArraySet set = new CharArraySet(Version.LUCENE_40, 0, false);
		return set;
	}
	
	protected CharArraySet readStopWords(String stopwordFile, boolean ignoreCase) throws IOException{
		int initSize = 1066;
		CharArraySet set = new CharArraySet(Version.LUCENE_40, initSize, ignoreCase);
		//read stopwords file
		BufferedReader in= new BufferedReader(new FileReader(stopwordFile));
		String line = null;
		while((line = in.readLine()) != null) {
			//each line is a stopword
			set.add(line);
		}
		in.close();
		return set;
	}
	
}
