package edu.northwestern.websail.wda.data.index;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Collector;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import edu.northwestern.websail.wda.model.TermMeasureStat;
import edu.northwestern.websail.wda.util.Measurement;

public class IndexDocReader {
	private DirectoryReader currentReader;


	private IndexSearcher searcher;
	
	public static void main (String[] args) throws IOException{
		IndexDocReader iddr = new IndexDocReader(args[0]);
		WordTitleStatCollector collector = new WordTitleStatCollector(iddr.getCurrentReader());
		ArrayList<Integer> a = new ArrayList<Integer>();
		a.add(6886);
		a.add(9959000);
		iddr.getDocuments(a, collector);
		System.out.println(collector.getTotalDocs());
		System.out.println(collector.getStat().size());
		System.out.println(collector.getStat());
	}
	
	public IndexDocReader(String indexDirectory) throws IOException {
		this.initializeReader(indexDirectory);
	}
	
	private void initializeReader(String indexDirectory) throws IOException {
		File indexFile = new File(indexDirectory);
	    Directory directory = FSDirectory.open(indexFile);
		currentReader = DirectoryReader.open(directory);
		searcher = new IndexSearcher(currentReader);
	}
	
	public void close() throws IOException{
		this.currentReader.close();
	}

	public void getDocuments(int titleId, Collector collector) throws IOException{
		Query q = NumericRangeQuery.newIntRange("id", titleId, titleId, true, true);
	    searcher.search(q, collector);
	}
	
	public void getDocuments(ArrayList<Integer> titleIds, Collector collector) throws IOException{
		BooleanQuery combined = new BooleanQuery();
		for(Integer titleId : titleIds){
			Query q = NumericRangeQuery.newIntRange("id", titleId, titleId, true, true);
			combined.add(q, BooleanClause.Occur.SHOULD);
		}
		searcher.search(combined, collector);
	}
	
	public DirectoryReader getCurrentReader() {
		return currentReader;
	}

	public void setCurrentReader(DirectoryReader currentReader) {
		this.currentReader = currentReader;
	}
	
	public static class WordTitleStatCollector extends Collector{
private DirectoryReader reader;
		
		private int totalDocs = 0;
    	private int base = 0;
    	private HashMap<String, TermMeasureStat> stat = new HashMap<String, TermMeasureStat>();
    	
    	public WordTitleStatCollector(DirectoryReader reader){
    		this.reader = reader;
    	}
    	
    	public WordTitleStatCollector(DirectoryReader reader, HashMap<String, TermMeasureStat> stat){
    		this.reader = reader;
    		this.stat = stat;
    	}
    	
		@Override
		public boolean acceptsDocsOutOfOrder() {
			return true;
		}

		@Override
		public void collect(int docId) throws IOException {
			totalDocs++;
			Terms vector = this.reader.getTermVector(docId+base, "article");
			if(vector == null) return;
			TermsEnum termsEnum = null;
	        termsEnum = vector.iterator(termsEnum);
	        BytesRef text = null;
	        while ((text = termsEnum.next()) != null) {
	            String term = text.utf8ToString();
	            long freq = termsEnum.totalTermFreq();
	            if(!this.stat.containsKey(term)) {
	            	TermMeasureStat tStat = new TermMeasureStat(term);
	            	stat.put(term, tStat);
	            }
	            stat.get(term).updateDF( freq);
	        }
		}

		@Override
		public void setNextReader(AtomicReaderContext context) throws IOException {
			this.base = context.docBase;
		}

		@Override
		public void setScorer(Scorer score) throws IOException {
			
		}

		public int getTotalDocs() {
			return totalDocs;
		}

		public HashMap<String, TermMeasureStat> getStat() {
			for(Entry<String, TermMeasureStat> entry : this.stat.entrySet()){
				Measurement.computeAllMeasures(entry.getValue(), totalDocs);
			}
			return stat;
		}

		public void setTotalDocs(int totalDocs) {
			this.totalDocs = totalDocs;
		}

		public void setStat(HashMap<String, TermMeasureStat> tf) {
			this.stat = tf;
		}
	}
	
	public static class WordTitleCollector extends Collector{
    	private DirectoryReader reader;
		
		private int totalDocs = 0;
    	private int base = 0;
    	private HashMap<String, Long> tf = new HashMap<String, Long>();
    	
    	public WordTitleCollector(DirectoryReader reader){
    		this.reader = reader;
    	}
    	
		@Override
		public boolean acceptsDocsOutOfOrder() {
			return true;
		}

		@Override
		public void collect(int docId) throws IOException {
			totalDocs++;
			Terms vector = this.reader.getTermVector(docId+base, "article");
			if(vector == null) return;
			TermsEnum termsEnum = null;
	        termsEnum = vector.iterator(termsEnum);
	        BytesRef text = null;
	        while ((text = termsEnum.next()) != null) {
	            String term = text.utf8ToString();
	            long freq = termsEnum.totalTermFreq();
	            if(this.tf.containsKey(term)) freq += this.tf.get(term);
	            tf.put(term, freq);
	        }
		}

		@Override
		public void setNextReader(AtomicReaderContext context) throws IOException {
			this.base = context.docBase;
		}

		@Override
		public void setScorer(Scorer score) throws IOException {
			
		}

		public int getTotalDocs() {
			return totalDocs;
		}

		public HashMap<String, Long> getTf() {
			return tf;
		}

		public void setTotalDocs(int totalDocs) {
			this.totalDocs = totalDocs;
		}

		public void setTf(HashMap<String, Long> tf) {
			this.tf = tf;
		}
		
		
    	
    }
	
	public static class DocCountCollector extends Collector{
		//private int base;
		private int totalDoc = 0;
		@Override
		public boolean acceptsDocsOutOfOrder() {
			return true;
		}

		@Override
		public void collect(int docId) throws IOException {
			
			totalDoc++;
		}

		@Override
		public void setNextReader(AtomicReaderContext context) throws IOException {
			//this.base = context.docBase;
		}

		@Override
		public void setScorer(Scorer score) throws IOException {
			//don't care
		}

		public int getTotalDoc() {
			return totalDoc;
		}

		public void setTotalDoc(int totalDoc) {
			this.totalDoc = totalDoc;
		}
		
	}
	
}
