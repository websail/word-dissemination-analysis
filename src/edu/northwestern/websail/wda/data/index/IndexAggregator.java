package edu.northwestern.websail.wda.data.index;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import edu.northwestern.websail.wda.data.Aggregator;

public class IndexAggregator extends Aggregator {
	
	FieldType fieldType;
	IndexWriter iwriter;

	public IndexAggregator(String language){
		super(language);
		this.fieldType =getIndexfieldType();
	}
	
	public IndexAggregator(String language, String stopWordsFile){
		super(language, stopWordsFile);
		this.fieldType =getIndexfieldType();
	}
	
	@Override
	public void initEmptyAggregator(String indexDirectory) throws IOException{
		//config lucene index writer
		Analyzer analyzer = null; 
		if(this.onlyWhiteSpace) {
			analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
		}
		else if(this.stopWordsFile == null)
			analyzer = new StandardAnalyzer(Version.LUCENE_40, emptyStopWords());
		else
			analyzer = new StandardAnalyzer(Version.LUCENE_40, this.readStopWords(this.stopWordsFile, true));
		File indexFile = new File(indexDirectory+"/"+lang);
	    Directory directory = FSDirectory.open(indexFile);
	    IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
	    this.iwriter = new IndexWriter(directory, config);
	}

    public void initExistingAggregator(String indexDirectory)  throws IOException{
        Analyzer analyzer= null;

        if(this.onlyWhiteSpace) {
			analyzer = new WhitespaceAnalyzer(Version.LUCENE_40);
		}
		else if(this.stopWordsFile == null)
			analyzer = new StandardAnalyzer(Version.LUCENE_40, emptyStopWords());
		else
			analyzer = new StandardAnalyzer(Version.LUCENE_40, this.readStopWords(this.stopWordsFile, true));
        File indexFile = new File(indexDirectory+"/"+lang);
        Directory directory = FSDirectory.open(indexFile);
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        this.iwriter = new IndexWriter(directory, config);
    }
	
	@Override
	public void close() throws IOException{
		iwriter.close();
	}
	
	@Override
	public void addPlaintextDocument(int pageId, String pageTitle, String content) throws IOException, InterruptedException {
		if(content.trim().equals("")) return;
		content = this.preprocessContent(content);
		Document doc = new Document();
	    doc.add(new IntField("id", pageId, Field.Store.YES));
	    doc.add(new StringField("title", pageTitle, Field.Store.YES));
	    doc.add(new Field("article", content, this.fieldType));
		this.iwriter.addDocument(doc);
		docCount++;
		if(docCount % 1000 == 0 && verbose) System.out.println("processing: " + docCount + ", skip: "+this.docSkip);
	}
	
	private FieldType getIndexfieldType() {
		FieldType vecType = new FieldType();
	    vecType.setIndexed(true);
	    vecType.setTokenized(true);
	    vecType.setStored(true);
	    vecType.setStoreTermVectors(true);
	    vecType.setStoreTermVectorPositions(true);
	    return vecType;
	}	
}
