package edu.northwestern.websail.wda.model;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;

import com.google.gson.Gson;

/**
 * User: csbhagav Date: 9/24/13
 */
public class CorpusStatLM extends CorpusStat {
	private static final long serialVersionUID = 4155027520742277701L;
	private HashMap<String, Long> keyPositionsMap;
	// private RandomAccessFile dataFile;
	private String dataFileName;

	public CorpusStatLM(String name, Integer totalDocs) {
		super(name, totalDocs);
	}

	public void setKeyPositionsMap(HashMap<String, Long> keyPositionsMap) {
		this.keyPositionsMap = keyPositionsMap;
	}

	public HashMap<String, Long> getKeyPositionsMap() {
		return keyPositionsMap;
	}

	// public void setDataFile(RandomAccessFile dataFile) {
	// this.dataFile = dataFile;
	// }

	public TermMeasureStat getTermMeasureStat(String key) throws IOException {
		if (!keyPositionsMap.containsKey(key))
			return null;
		RandomAccessFile dataFile = new RandomAccessFile(
				new File(dataFileName), "r");
		dataFile.seek(keyPositionsMap.get(key));
		String[] parts = dataFile.readLine().split("\t");
		Gson gson = new Gson();
		dataFile.close();
		return gson.fromJson(parts[1], TermMeasureStat.class);
	}

	public String getDataFileName() {
		return dataFileName;
	}

	public void setDataFileName(String dataFileName) {
		this.dataFileName = dataFileName;
	}

}
