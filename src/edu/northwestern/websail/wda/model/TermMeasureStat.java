package edu.northwestern.websail.wda.model;

public class TermMeasureStat extends TermStat {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8052573077676294615L;
	private Double idf;
	private Double rIDF;
	private Double idf2;
	private Double rIDF2;
	private Double adapt2;
	private Double adapt3;
	private Double adapt4;
	private Double burstiness;
	
	public TermMeasureStat(String term) {
		super(term);
	}
	
	public TermMeasureStat(){}
	
	public TermMeasureStat(String term, long tf) {
		super(term,tf);
	}

	public TermMeasureStat(TermStat t){
		super(t);
	}
	
	public Double getIdf() {
		return idf;
	}

	public Double getrIDF() {
		return rIDF;
	}

	public Double getAdapt2() {
		return adapt2;
	}

	public Double getAdapt3() {
		return adapt3;
	}

	public void setIdf(Double idf) {
		this.idf = idf;
	}

	public void setrIDF(Double rIDF) {
		this.rIDF = rIDF;
	}

	public void setAdapt2(Double adapt2) {
		this.adapt2 = adapt2;
	}

	public void setAdapt3(Double adapt3) {
		this.adapt3 = adapt3;
	}

	public Double getBurstiness() {
		return burstiness;
	}

	public void setBurstiness(Double burstiness) {
		this.burstiness = burstiness;
	}

	public Double getAdapt4() {
		return adapt4;
	}

	public void setAdapt4(Double adapt4) {
		this.adapt4 = adapt4;
	}

	public Double getIdf2() {
		return idf2;
	}

	public Double getrIDF2() {
		return rIDF2;
	}

	public void setIdf2(Double idf2) {
		this.idf2 = idf2;
	}

	public void setrIDF2(Double rIDF2) {
		this.rIDF2 = rIDF2;
	}

	public String toString(){
		return this.getTerm() + " (" + this.getTf() + "/" + this.getIdf() + ")";
	}
	
}
