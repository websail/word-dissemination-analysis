package edu.northwestern.websail.wda.model;

import java.io.Serializable;

public class TermStat implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3417327564929523132L;
	protected String term;
	protected Integer df1;
	protected Integer df2;
	protected Integer df3;
	protected Integer df4;
	protected Integer df5;
	protected boolean isTFGiven;
	protected Long tf;
	
	public TermStat(String term){
		this.term = term;
		this.df1 = 0;
		this.df2 = 0;
		this.df3 = 0;
		this.df4 = 0;
		this.df5 = 0;
		this.isTFGiven = false;
		this.tf = 0l;
	}
	
	public TermStat(){}
	
	public TermStat(String term, long tf) {
		this(term);
		this.isTFGiven = true;
		this.tf = tf;
	}
	
	public TermStat(TermStat t) {
		this.term = t.getTerm();
		this.df1 = t.getDf1();
		this.df2 = t.getDf2();
		this.df3 = t.getDf3();
		this.df4 = t.getDf4();
		this.df5 = t.getDf5m();
		this.tf = t.getTf();
		this.isTFGiven = true;
	}
	
	public void updateDF(long docTF){
		if(!isTFGiven) this.tf+=docTF;
		if(docTF > 4) this.df5++;
		if(docTF > 3) this.df4++;
		if(docTF > 2) this.df3++;
		if(docTF > 1) this.df2++;
		if(docTF > 0) this.df1++;
	}
	
	public String getTerm() {
		return term;
	}
	public Integer getDf1() {
		return df1;
	}
	public Integer getDf2() {
		return df2;
	}
	public Integer getDf3() {
		return df3;
	}
	public Integer getDf4() {
		return df4;
	}
	public Integer getDf5m() {
		return df5;
	}
	public Long getTf() {
		return tf;
	}

	public Integer getDf5() {
		return df5;
	}

	public boolean isTFGiven() {
		return isTFGiven;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public void setDf1(Integer df1) {
		this.df1 = df1;
	}

	public void setDf2(Integer df2) {
		this.df2 = df2;
	}

	public void setDf3(Integer df3) {
		this.df3 = df3;
	}

	public void setDf4(Integer df4) {
		this.df4 = df4;
	}

	public void setDf5(Integer df5) {
		this.df5 = df5;
	}

	public void setTFGiven(boolean isTFGiven) {
		this.isTFGiven = isTFGiven;
	}

	public void setTf(Long tf) {
		this.tf = tf;
	}
	
}
