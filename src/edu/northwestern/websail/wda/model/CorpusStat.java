package edu.northwestern.websail.wda.model;

import edu.northwestern.websail.wda.util.Measurement;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;


public class CorpusStat implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 4155027520742277701L;
    protected HashMap<String, TermMeasureStat> terms;
    protected Integer totalDocs;
    protected Integer totalTerms;
    protected Long totalTermFrequency;

    protected String name;

    public CorpusStat(String name, int totalDocs) {
        this.totalDocs = totalDocs;
        this.name = name;
        this.terms = new HashMap<String, TermMeasureStat>();
        this.totalTerms = 0;
        this.totalTermFrequency = 0l;
    }

    public void addTerm(String term, long tf) {
        TermMeasureStat t = new TermMeasureStat(term, tf);
        Measurement.computeAllMeasures(t, this.totalDocs);
        this.terms.put(term, t);
        this.totalTerms++;
        this.totalTermFrequency += tf;
    }

    public void addTerm(TermStat t) {
        TermMeasureStat tM = new TermMeasureStat(t);
        Measurement.computeAllMeasures(tM, this.totalDocs);
        this.terms.put(t.getTerm(), tM);
        this.totalTerms++;
        this.totalTermFrequency += t.getTf();
    }

    public TermMeasureStat getTermMeasureStat(String key) throws IOException {
        return terms.get(key);
    }

    public HashMap<String, TermMeasureStat> getTerms() {
        return terms;
    }

    public void setTerms(HashMap<String, TermMeasureStat> terms) {
		this.terms = terms;
	}

	public void setTotalDocs(Integer totalDocs) {
		this.totalDocs = totalDocs;
	}

	public Integer getTotalDocs() {
        return totalDocs;
    }

    public Integer getTotalTerms() {
        return totalTerms;
    }

    public void setTotalTerms(Integer totalTerms) {
        this.totalTerms = totalTerms;
    }

    public Long getTotalTermFrequency() {
        return totalTermFrequency;
    }

    public void setTotalTermFrequency(Long totalTermFrequency) {
        this.totalTermFrequency = totalTermFrequency;
    }

    public String getName() {
        return name;
    }
}
