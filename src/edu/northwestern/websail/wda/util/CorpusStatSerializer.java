package edu.northwestern.websail.wda.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;

import com.google.gson.Gson;

import edu.northwestern.websail.wda.model.CorpusStat;


public class CorpusStatSerializer {
	public static CorpusStat deserialize(String filename) throws IOException {
		Gson gson = new Gson();
		BufferedReader in= new BufferedReader(new FileReader(filename));
		CorpusStat stat = gson.fromJson(in, CorpusStat.class);
		in.close();
		return stat;
//		
//		ObjectMapper mapper = new ObjectMapper();
//		CorpusStat user = mapper.readValue(new File(filename), CorpusStat.class);
//		return user;
	}
	
	public static void serialize(CorpusStat stat, String filename) throws IOException{
		Gson gson = new Gson();
		BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
			    new FileOutputStream(filename), "UTF-8"));
		gson.toJson(stat, out);
		out.flush();
		out.close();
//		
//		ObjectMapper mapper = new ObjectMapper();
//		mapper.writeValue(new File(filename), stat);
	}
	
	
	public static CorpusStat deserializeBinary(String filename) throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename));
		Object object = ois.readObject();
		ois.close();
		return (CorpusStat) object;
	}
	
	public static void serializeBinary(CorpusStat stat, String filename) throws IOException{
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename));
		oos.writeObject(stat);
		oos.flush();
		oos.close();
	}
}
