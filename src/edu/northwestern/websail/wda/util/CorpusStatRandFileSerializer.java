package edu.northwestern.websail.wda.util;

import com.google.gson.Gson;
import edu.northwestern.websail.wda.model.CorpusStatLM;
import edu.northwestern.websail.wda.model.CorpusStat;
import edu.northwestern.websail.wda.model.TermMeasureStat;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * User: csbhagav Date: 9/24/13
 */
public class CorpusStatRandFileSerializer {

	public static void serializeToRandomAccessFiles(CorpusStat statObj,
			String dirName, String statFileName) throws IOException {

		File dataFile = new File(dirName, statFileName);
		File positionFile = new File(dirName, statFileName + "-positions");
		File metaDataFile = new File(dirName, statFileName + "-metadata");

		BufferedWriter out = new BufferedWriter(new FileWriter(metaDataFile));
		BufferedWriter positionFileWriter = new BufferedWriter(new FileWriter(
				positionFile));
		RandomAccessFile dataFileWriter = new RandomAccessFile(dataFile, "rw");

		out.write("totalDocs\t" + statObj.getTotalDocs() + "\n");
		out.write("totalTerms\t" + statObj.getTotalTerms() + "\n");
		out.write("totalTermFrequency\t" + statObj.getTotalTermFrequency()
				+ "\n");
		out.write("name\t" + statObj.getName() + "\n");

		HashMap<String, TermMeasureStat> terms = statObj.getTerms();
		Gson gson = new Gson();

		for (Map.Entry<String, TermMeasureStat> term : terms.entrySet()) {
			positionFileWriter.write(term.getKey() + "\t"
					+ dataFileWriter.getChannel().position() + "\n");
			String statStr = gson.toJson(term.getValue());
			String writeStr = term.getKey() + "\t" + statStr + "\n";
			dataFileWriter.write(writeStr.getBytes());

		}

		out.close();
		dataFileWriter.close();
		positionFileWriter.close();
	}

	public static CorpusStatLM deserializeFromRandomAccessFile(String dirName,
			String statFileName) throws IOException {

		File dataFile = new File(dirName, statFileName);
		File positionFile = new File(dirName, statFileName + "-positions");
		File metaDataFile = new File(dirName, statFileName + "-metadata");

		BufferedReader in = new BufferedReader(new FileReader(metaDataFile));
		String line;

		Integer totalDocs = 0;
		Integer totalTerms = 0;
		Long totalTermFrequency = 0l;
		String name = "";
		while ((line = in.readLine()) != null) {

			String[] parts = line.split("\t");

			if (parts[0].equalsIgnoreCase("totalDocs")) {
				totalDocs = Integer.valueOf(parts[1]);
			} else if (parts[0].equalsIgnoreCase("totalTerms")) {
				totalTerms = Integer.valueOf(parts[1]);
			} else if (parts[0].equalsIgnoreCase("totalTermFrequency")) {
				totalTermFrequency = Long.valueOf(parts[1]);
			} else if (parts[0].equalsIgnoreCase("name")) {
				name = parts[1];
			}
		}

		CorpusStatLM statObj = new CorpusStatLM(name, totalDocs);
		statObj.setTotalTerms(totalTerms);
		statObj.setTotalTermFrequency(totalTermFrequency);

		in.close();

		in = new BufferedReader(new FileReader(positionFile));
		HashMap<String, Long> keyPositionsMap = new HashMap<String, Long>();
		while ((line = in.readLine()) != null) {
			String[] parts = line.split("\t");
			keyPositionsMap.put(parts[0], Long.valueOf(parts[1]));
		}
		in.close();

		statObj.setKeyPositionsMap(keyPositionsMap);
		statObj.setDataFileName(dataFile.getAbsolutePath());
		return statObj;
	}

}
